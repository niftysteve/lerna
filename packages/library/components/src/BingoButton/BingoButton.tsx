import React from "react";

import { Button, ButtonProps } from '@mui/material';

const BingoButton = ({ ...rest}: ButtonProps ) => (
  <Button {...rest} />
)

export default BingoButton;

import React from 'react'

import { Typography, TypographyProps } from '@mui/material';

const BingoText = ({ ...rest }: TypographyProps ) => (
  <Typography {...rest} />
)

export default BingoText;

import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import svgr from '@svgr/rollup';
import deepmerge from 'deepmerge'
import external from 'rollup-plugin-peer-deps-external';
import { terser } from 'rollup-plugin-terser';

export default (args) => {
  const defaultOpts = {
    output: [
      {
        dir: 'dist',
        format: 'cjs',
        sourcemap: true,
      },
    ],
    plugins: [
      external(['react', 'react-dom', '@mui', '@emotion']),
      resolve(),
      commonjs(),
      typescript(),
      terser(),
      svgr(),
    ],
  }

  return deepmerge(defaultOpts, args)
};

import React from 'react';

import { BingoButton,BingoText } from "@bingotest/components"
import { Stack } from '@mui/material';

const App = () => (
    <Stack>
    <BingoButton>Hello</BingoButton>
    <BingoText>Apples</BingoText>
  </Stack>
  )

export default App;

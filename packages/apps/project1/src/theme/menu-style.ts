import { ThemeOptions } from '@mui/material/styles';

const menuStyle: ThemeOptions = {
  components: {
    MuiMenuItem: {
      styleOverrides: {
        root: ({ theme }) => ({
          height: 50,
          '&.Mui-selected': {
            backgroundColor: `${theme.palette.action.hover} !important`,
            ':hover': {
              backgroundColor: theme.palette.action.hover,
            },
          },
        }),
      },
    },
    MuiAutocomplete: {
      styleOverrides: {
        endAdornment: {
          top: 'unset',
        },
      },
    },
  },
};

export default menuStyle;

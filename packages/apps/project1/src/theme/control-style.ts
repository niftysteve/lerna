import { ThemeOptions } from '@mui/material/styles';

const controlStyle: ThemeOptions = {
  components: {
    MuiFormLabel: {
      defaultProps: {
        focused: false,
      },
    },
    MuiFormControlLabel: {
      styleOverrides: {
        root: ({ theme, ownerState }) => ({
          '& > .Mui-checked': {
            ':hover': {
              ...(ownerState.color === 'primary' && {
                backgroundColor: theme.palette.primary.states.outlinedHoverBackground,
              }),
              ...(ownerState.color === 'secondary' && {
                backgroundColor: theme.palette.secondary.states.outlinedHoverBackground,
              }),
            },
          },
        }),
      },
    },
    MuiRadio: {
      defaultProps: {
        color: 'secondary',
      },
      styleOverrides: {
        root: ({ theme, ownerState }) => ({
          ':hover': {
            ...(!ownerState.checked && {
              backgroundColor: theme.palette.action.hover,
            }),
          },
        }),
        colorPrimary: ({ theme, ownerState }) => ({
          ':hover': {
            ...(ownerState.checked && {
              backgroundColor: theme.palette.primary.states.outlinedHoverBackground,
            }),
          },
        }),
        colorSecondary: ({ theme, ownerState }) => ({
          ':hover': {
            ...(ownerState.checked && {
              backgroundColor: theme.palette.secondary.states.outlinedHoverBackground,
            }),
          },
        }),
      },
    },
    MuiCheckbox: {
      defaultProps: {
        color: 'secondary',
      },
      styleOverrides: {
        root: ({ theme, ownerState }) => ({
          ':hover': {
            ...(!ownerState.checked && {
              backgroundColor: theme.palette.action.hover,
            }),
          },
        }),
        colorPrimary: ({ theme, ownerState }) => ({
          ':hover': {
            ...(ownerState.checked && {
              backgroundColor: theme.palette.primary.states.outlinedHoverBackground,
            }),
          },
        }),
        colorSecondary: ({ theme, ownerState }) => ({
          ':hover': {
            ...(ownerState.checked && {
              backgroundColor: theme.palette.secondary.states.outlinedHoverBackground,
            }),
          },
        }),
      },
    },
  },
};

export default controlStyle;

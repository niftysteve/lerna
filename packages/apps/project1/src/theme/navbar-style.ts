import { ThemeOptions } from '@mui/material/styles';

const navbarStyle: ThemeOptions = {
  components: {
    MuiAppBar: {
      defaultProps: {
        color: 'default',
        position: 'sticky',
        elevation: 2,
      },
      styleOverrides: {
        root: {
          justifyContent: 'center',
          minHeight: 82,
          backgroundColor: '#FFFFFF',
        },
      },
    },
    MuiBreadcrumbs: {
      styleOverrides: {
        root: {
          marginTop: '26px',
          marginLeft: '25px',
          paddingBottom: '25px',
        },
      },
    },
  },
};

export default navbarStyle;

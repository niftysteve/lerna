declare module '@mui/material/styles' {
  interface Palette {
    grey11: Palette['primary'];
  }
  interface PaletteOptions {
    grey11: PaletteOptions['primary'];
  }
}

export {};

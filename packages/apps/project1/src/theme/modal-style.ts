import { ThemeOptions } from '@mui/material/styles';

const modalStyle: ThemeOptions = {
  components: {
    MuiDialog: {
      styleOverrides: {
        paper: {
          minWidth: '564px',
        },
      },
    },
    MuiDialogTitle: {
      styleOverrides: {
        root: ({ theme }) => ({
          padding: '8px 16px',
          ...theme.typography.h5,
        }),
      },
    },
    MuiDialogContent: {
      styleOverrides: {
        root: {
          padding: '16px',
        },
      },
    },
    MuiDialogActions: {
      styleOverrides: {
        root: {
          padding: '16px',
        },
      },
    },
  },
};

export default modalStyle;

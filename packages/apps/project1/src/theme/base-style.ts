import { ThemeOptions } from '@mui/material/styles';

const baseStyle: ThemeOptions = {
  palette: {
    mode: 'light',
    primary: {
      main: '#42c7e5',
      light: '#d9f5fa',
      dark: '#3cadc7',
      contrastText: '#0a3670',
      states: {
        outlinedHoverBackground: '#42c7e514',
        outlinedRestingBorder: '#42c7e580',
      },
    },
    secondary: {
      main: '#1282a3',
      light: '#4ab3c9',
      dark: '#216373',
      contrastText: '#ffffff',
      states: {
        outlinedHoverBackground: '#1282a31f',
        outlinedRestingBorder: '#1282a380',
      },
    },
    info: {
      main: '#0a3670',
      light: '#6b87a8',
      dark: '#082954',
      contrastText: '#ffffff',
      states: {
        outlinedHoverBackground: '#0a367080',
        outlinedRestingBorder: '#0a367014',
      },
    },
    grey11: {
      main: '#ccccc9',
      light: '#efeff0',
      dark: '#4c4c48',
      contrastText: '#0a3670',
      states: {
        outlinedHoverBackground: '#ccccc91f',
        outlinedRestingBorder: '#ccccc980',
      },
    },
    success: {
      main: '#4caf50',
      light: '#9dcf91',
      dark: '#368529',
      contrastText: '#ffffff',
      states: {
        outlinedHoverBackground: '#68b55680',
        outlinedRestingBorder: '#68b55614',
      },
    },
    error: {
      main: '#b20033',
      light: '#ff80a4',
      dark: '#800025',
      contrastText: '#ffffff',
      states: {
        outlinedHoverBackground: '#b2003314',
        outlinedRestingBorder: '#b2003380',
      },
    },
    warning: {
      main: '#ed6c02',
      light: '#ffb547',
      dark: '#c77700',
      states: {
        outlinedHoverBackground: '#ed6c0280',
        outlinedRestingBorder: '#ed6c0214',
      },
    },
    action: {
      active: '#171a1a8a',
      hover: '#171a1a0a',
      selected: '#171a1a14',
      disabled: '#171a1a42',
      disabledBackground: '#171a1a1f',
      focus: '#171a1a0a',
    },
    text: {
      primary: '#171a1a',
      secondary: '#171a1a99',
      disabled: '#171a1a61',
    },
    background: {
      paper: '#ffffff',
    },
    divider: '#171a1a1f',
  },
  typography: {
    fontSize: 16,
    fontFamily: `"Helvetica Neue", sans-serif`,
    h1: {
      fontSize: 34,
      lineHeight: '40px',
      letterSpacing: '.25px',
      fontWeight: 400,
    },
    h2: {
      fontSize: 24,
      lineHeight: '32px',
      letterSpacing: '.25px',
      fontWeight: 500,
    },
    h3: {
      fontSize: 22,
      lineHeight: '30px',
      letterSpacing: '.15px',
      fontWeight: 700,
    },
    h4: {
      fontSize: 20,
      lineHeight: '24px',
      letterSpacing: '.15px',
      fontWeight: 500,
    },
    h5: {
      fontSize: 18,
      lineHeight: '24px',
      letterSpacing: '.15px',
      fontWeight: 500,
    },
    h6: {
      fontSize: 16,
      lineHeight: '22px',
      letterSpacing: '.15px',
      fontWeight: 500,
    },
    subtitle1: {
      fontSize: 16,
      lineHeight: '20px',
      letterSpacing: '.15px',
      fontWeight: 400,
    },
    subtitle2: {
      fontSize: 14,
      lineHeight: '20px',
      letterSpacing: '.15px',
      fontWeight: 500,
    },
    body1: {
      fontSize: 16,
      lineHeight: '22px',
      letterSpacing: '.2px',
      fontWeight: 400,
    },
    body2: {
      fontSize: 14,
      lineHeight: '20px',
      letterSpacing: '.2px',
      fontWeight: 400,
    },
    caption: {
      fontSize: 14,
      lineHeight: '16px',
      letterSpacing: '.4px',
      fontWeight: 400,
    },
    overline: {
      fontSize: 12,
      lineHeight: '16px',
      letterSpacing: '1.5px',
      fontWeight: 500,
    },
  },
};

export default baseStyle;

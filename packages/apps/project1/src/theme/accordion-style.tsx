import React from 'react';

import { Icon } from '@mui/material';
import { ThemeOptions } from '@mui/material/styles';

const accordionStyle: ThemeOptions = {
  components: {
    MuiAccordionSummary: {
      defaultProps: {
        expandIcon: <Icon>expand_more</Icon>,
      },
      styleOverrides: {
        root: ({ theme }) => ({
          height: 67,
          [theme.breakpoints.up('lg')]: {
            height: 71,
          },
          '&.Mui-expanded': {
            '&.MuiAccordionSummary-root': {
              borderBottom: `1px solid ${theme.palette.divider}`,
            },
          },
        }),
      },
    },
  },
};

export default accordionStyle;

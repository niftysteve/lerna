import { ThemeOptions } from '@mui/material/styles';

declare module '@mui/material/Button' {
  interface ButtonPropsColorOverrides {
    grey11: true;
  }
}

declare module '@mui/material/IconButton' {
  interface IconButtonPropsColorOverrides {
    grey11: true;
  }
}

const buttonStyle: ThemeOptions = {
  components: {
    MuiButtonBase: {
      defaultProps: {
        disableRipple: true,
        disableTouchRipple: true,
      },
    },
    MuiButton: {
      defaultProps: {
        variant: 'contained',
      },
      styleOverrides: {
        root: {
          minWidth: '48px',
          letterSpacing: '0.46px',
          textTransform: 'capitalize',
          fontWeight: 500,
        },
        sizeSmall: {
          padding: '4px 10px',
          lineHeight: '22px',
          fontSize: 13,
        },
        sizeMedium: {
          padding: '6px 16px',
          lineHeight: '24px',
          fontSize: 14,
        },
        sizeLarge: {
          padding: '8px 22px',
          lineHeight: '26px',
          fontSize: 15,
        },
        contained: ({ ownerState, theme }) => ({
          ...(ownerState.color === 'grey11' && {
            ':hover': {
              backgroundColor: theme.palette.action.active,
            },
          }),
        }),
        outlined: ({ ownerState, theme }) => ({
          ...(ownerState.color === 'primary' && {
            color: theme.palette.primary.contrastText,
            borderColor: theme.palette.primary.states.outlinedRestingBorder,
            ':hover': {
              backgroundColor: theme.palette.primary.states.outlinedHoverBackground,
            },
          }),
          ...(ownerState.color === 'secondary' && {
            borderColor: theme.palette.secondary.states.outlinedRestingBorder,
            ':hover': {
              backgroundColor: theme.palette.secondary.states.outlinedHoverBackground,
            },
          }),
          ...(ownerState.color === 'grey11' && {
            color: theme.palette.grey11.contrastText,
            borderColor: theme.palette.grey11.states.outlinedRestingBorder,
            ':hover': {
              backgroundColor: theme.palette.grey11.states.outlinedHoverBackground,
            },
          }),
        }),
        text: ({ ownerState, theme }) => ({
          ...(ownerState.color === 'primary' && {
            color: theme.palette.primary.contrastText,
            ':hover': {
              backgroundColor: theme.palette.primary.states.outlinedHoverBackground,
            },
          }),
          ...(ownerState.color === 'secondary' && {
            ':hover': {
              backgroundColor: theme.palette.secondary.states.outlinedHoverBackground,
            },
          }),
          ...(ownerState.color === 'grey11' && {
            color: theme.palette.grey11.contrastText,
            ':hover': {
              backgroundColor: theme.palette.grey11.states.outlinedHoverBackground,
            },
          }),
        }),
      },
    },
    MuiIconButton: {
      defaultProps: {
        color: 'secondary',
      },
      styleOverrides: {
        root: ({ theme, ownerState }) => ({
          borderRadius: 0,
          '& > *:first-of-type': {
            fontSize: 'inherit',
          },
          ...(ownerState.color === 'grey11' && {
            color: theme.palette.grey11.dark,
          }),
          ':hover': {
            ...(ownerState.color === 'primary' && {
              backgroundColor: theme.palette.primary.states.outlinedHoverBackground,
            }),
            ...(ownerState.color === 'secondary' && {
              backgroundColor: theme.palette.secondary.states.outlinedHoverBackground,
            }),
            ...(ownerState.color === 'grey11' && {
              backgroundColor: theme.palette.action.hover,
            }),
          },
          ':active': {
            ...(ownerState.color === 'info' && {
              backgroundColor: theme.palette.action.selected,
            }),
            ...(ownerState.color === 'grey11' && {
              backgroundColor: theme.palette.action.selected,
            }),
          },
        }),
        sizeSmall: {
          width: 28,
          height: 28,
        },
        sizeMedium: {
          width: 48,
          height: 48,
        },
        sizeLarge: {
          width: 58,
          height: 58,
        },
      },
    },
  },
};

export default buttonStyle;

import { ThemeOptions } from '@mui/material/styles';

const tabStyle: ThemeOptions = {
  components: {
    MuiTabs: {
      defaultProps: {
        textColor: 'inherit',
        variant: 'scrollable',
        allowScrollButtonsMobile: true,
      },
      styleOverrides: {
        root: ({ theme }) => ({
          borderBottom: `1px solid ${theme.palette.divider}`,
          padding: '0 30px',
        }),
        vertical: ({ theme }) => ({
          borderBottom: 'unset',
          borderRight: `1px solid ${theme.palette.divider}`,
          padding: '30px 0',
        }),
        flexContainer: {
          columnGap: '30px',
        },
        flexContainerVertical: {
          '& > *[role=tab]': {
            alignSelf: 'center',
            width: '100%',
          },
        },
      },
    },
    MuiTab: {
      styleOverrides: {
        root: {
          padding: '12px 8px',
          textTransform: 'capitalize',
        },
      },
    },
  },
};

export default tabStyle;

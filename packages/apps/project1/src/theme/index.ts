import { createTheme } from '@mui/material/styles';

import accordionStyle from './accordion-style';
import baseStyle from './base-style';
import buttonStyle from './button-style';
import controlStyle from './control-style';
import menuStyle from './menu-style';
import modalStyle from './modal-style';
import navbarStyle from './navbar-style';
import progressStyle from './progress-style';
import tabStyle from './tab-style';

const theme = createTheme(
  baseStyle,
  accordionStyle,
  buttonStyle,
  controlStyle,
  tabStyle,
  navbarStyle,
  menuStyle,
  modalStyle,
  progressStyle,
);

export default theme;

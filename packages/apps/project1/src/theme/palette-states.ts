declare module '@mui/material/styles' {
  interface PaletteColor {
    states: {
      outlinedHoverBackground?: string;
      outlinedRestingBorder?: string;
    };
  }
  interface SimplePaletteColorOptions {
    states: {
      outlinedHoverBackground?: string;
      outlinedRestingBorder?: string;
    };
  }
}

export {};

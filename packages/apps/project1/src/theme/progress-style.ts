import { ThemeOptions } from '@mui/material/styles';

const progressStyle: ThemeOptions = {
  components: {
    MuiCircularProgress: {
      defaultProps: {
        color: 'secondary',
      },
    },
  },
};

export default progressStyle;
